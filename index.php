<?php 
	session_start();
	if (isset($_GET['username'])) 
	{ 
		$_SESSION['username'] = $_GET['username'];
	}
	include "init.php";
	include "connectDBChat.php";
	include "functions.php";
	include "get_data.php";
	include CHAT_FILE_SESSIONS;
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="cache-control" content="no-cache" />
	<meta charset="utf-8">
	<title>Chat</title>
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/chat.css">
	<link rel="stylesheet" type="text/css" href="css/typing-indicator.css">

	<script type="text/javascript" src="<?= SRC ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?= SRC ?>js/bootstrap.js"></script>
	<script type="text/javascript" src="<?= SRC ?>js/prototypes.js"></script>	
	<?php if ($CHAT_ALLOW_INIT) { ?>
	<script type="text/javascript" src="<?= SRC ?>js/chat.js"></script>
	<?php }; ?>
</head>
<body style="height: 1000px;">
	<?php if ($CHAT_ALLOW_INIT) { ?>
	<div class="ec-chat-open-teaser"><i class="fa fa-comments"></i> CHAT</div>
	<div class="ec-chat-placeholder">
		<div class="ec-chat-profile-section">
			<div class="row">
				<div class="col-md-6">
					<div class="ec-chat-user-status btn btn-chat-hover change-network-status not-available" 
					data-toggle="tooltip" data-placement="bottom" title="Budi nedostupan">
						<div class="ec-chat-user-available available" data-status="online">
							<span><div class="ec-chat-user-available-sign available"></div> Na mreži</span>
						</div>
						<div class="ec-chat-user-available not-available" data-status="offline">
							<span><span class="not-available"><i class="fa fa-times"></i> </span> Nedostupan</span>					
						</div>					
					</div>
				</div>				
				<div class=" col-md-6 text-right">
					<div class="ec-chat-user-status" data-toggle="tooltip" data-placement="bottom" title="Aktivni korisnici">
						<span class="ec-chat-users-available">
						   <span class="ec-chat-users-available-loader"><i class="fa fa-circle-o-notch fa-spin"></i></span>
						   <span class="ec-chat-users-available-number"></span>
						   <i class="fa fa-users"></i>
						</span>
					</div>				
					<div class="ec-chat-user-status btn btn-chat-hover close-ec-chat-placeholder" data-toggle="tooltip" data-placement="bottom" title="Zatvaranje">
						<i class="fa fa-arrow-right"></i>
					</div>		
				</div>				
			</div>	
			<img class="ec-chat-mainhead-profile-picture" src="<?= get_profile_picture($USER); ?>">	
			<a href="javascript: void(0);" class="test-link" style="display: none;"></a>	
		</div>
		<div class="ec-chat-status-bar"></div>

		<!-- Append Chat Users -->
		<div class="ec-chat-main-container"></div>
		<!-- /Append Chat Users -->

		<div class="ec-chat-empty-search-result">
			<div><i class="fa fa-cubes fa-5x"></i></div>
			<div class="text">Nema rezultata za <div class="ec-chat-search-result-phrase strong"></div></div>
		</div>

		<div class="ec-chat-search-placeholder">			
			<div class="ec-chat-search-items">
				<span><i class="fa fa-search"></i></span>
				<input class="ec-chat-search" type="text" placeholder="Pretražite korisnike">
				<div class="ec-chat-settings">
					<i class="fa fa-cog" data-toggle="tooltip" data-placement="bottom" title="Podešavanja"></i>
					<div class="dropup-menu">
						<div class="menu-option" data="sound">
							<span class="option"><i class="fa fa-check"></i></span>
							<span class="text">Zvuk razgovora</span>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="ec-chat-loading">
			<div class="content"><i class="fa fa-circle-o-notch fa-spin"></i></div>
		</div>
	</div>

	<div class="ec-chat-heads"></div>

	<div class="ec-chat-more-users">
		<div class="toggle-more-users">
			<i class="fa fa-comments-o fa-2x"></i>
			<div class="ec-chat-mu-number"></div>			
		</div>		
		<div class="ec-chat-mu-list">
			<div class="heading">Aktivni Razgovori</div>
			<div class="list"></div>
		</div>
	</div>

	<?php } else { ?>
	<div class="container" style="margin-top: 20px;">
		<div class="row">
			<div class="col-md-12 text-center">
				<h3><i class="fa fa-exclamation-triangle"></i> Nije moguće aktivirati čet uslugu</h3>
				<div><?= $CHAT_ERROR_INIT ?></div>
			</div>
		</div>
	</div>
	<?php }; ?>
</body>
</html>
<?php mysqli_close($connection); ?>